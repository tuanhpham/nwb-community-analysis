get-staged-ndx-GH-PR:
	curl "https://api.github.com/repos/nwb-extensions/staged-extensions/pulls?per_page=100&state=all" \
		-o data/github/staged-ndx-GH-PR.json

get-staged-ndx-PR-files:
	bash src/get-staged-ndx-PR-files.sh

scrape-dandi-meta:
	python src/scrape_dandi_meta.py

query-dandiset-on-googlescholar:
	python src/query_dandiset_on_scholar.py

query-dandiset-on-datacite:
	curl "https://api.datacite.org/events?prefix=10.48324,10.80507" \
		-o data/datacite/dandiset-datacite-query.json

