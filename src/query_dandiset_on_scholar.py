import glob
import os
import re
import json
import numpy as np
import pandas as pd
from tqdm import tqdm
from scholarly import scholarly, ProxyGenerator

def load_jsons(data_directory, glob_pattern):
    data = []
    for file in sorted(glob.glob(data_directory + '/' + glob_pattern + '*.json')):
        with open(file, 'r') as f:
            data.extend(json.load(f))
    return data

if __name__ == "__main__":
    raw_path = 'data/dandiarchive/raw'
    out_path = 'data/googlescholar/dandiset-scholar-query.json'
    simplified_out = 'data/googlescholar/dandiset-scholar-pubs.csv'
    
    pg = ProxyGenerator()
    
    # gather dandiset IDs
    meta_df = pd.DataFrame(load_jsons(raw_path, 'metadata'))
    meta_df = pd.json_normalize(meta_df['response'], sep='_')
    doi_list = meta_df['doi'].dropna().drop_duplicates().to_list()
    dandiset_IDs = [
        '%s' %(re.search('dandi.+', x).group())
        for x in doi_list
    ]
    
    # use scholarly to query
    pubs = dict()

    for dandiset in tqdm(dandiset_IDs):
        if dandiset in pubs:
            # in case need to restart
            continue

        pg.FreeProxies()
        scholarly.use_proxy(pg)
        query_results = scholarly.search_pubs(dandiset)

        pubs[dandiset] = pd.DataFrame([
            next(query_results) for _ in range(query_results.total_results)
        ]).assign(dandiset = dandiset)

    # save data
    df_pubs = pd.concat(list(pubs.values()), ignore_index=True)
    df_pubs.to_json(out_path, orient='records')
    
    # process + simplify
    df = pd.concat([
        (
            df_pubs
            .assign(dandiset_relation='found_in_pub')
            .rename(columns={'num_citations': 'pub_ncite'})
            .filter(['dandiset', 'dandiset_relation', 'pub_url', 'pub_ncite'])
            .astype({'pub_ncite': 'int'})
        ),
        (
            pd.json_normalize(df_pubs['bib'])
            .add_prefix('pub_')
            .rename(columns={'pub_pub_year': 'pub_year'})
        )
    ], axis=1)
    
    # save
    df.to_csv(simplified_out, index=False)
