#!/bin/bash

jq -r .[].url data/github/staged-ndx-GH-PR.json | while read -d $'\n' pr_url
do
  pr_files_url="$pr_url/files"
  pr_files_out="data/github/staged-ndx_PR-${pr_url##*/}_files.json"
  curl "$pr_files_url" -o "$pr_files_out"
  echo "$pr_files_url to $pr_files_out"
  sleep 1
done

