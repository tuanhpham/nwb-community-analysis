import os
import re
import json
import pickle
import pandas as pd

import asyncio

import rich
from tqdm import tqdm

from scrape import scrape


def save_data(data, file_prefix, data_directory, to_pkl=True, to_json=True):
    if to_json:
        file_path = os.path.join(data_directory, file_prefix + '.json')
        with open(file_path, "w") as f:
            json.dump(data, f)
        rich.print(f'\t Saved file: [italic yellow]{file_path}[/]')
    if to_pkl:
        file_path = os.path.join(data_directory, file_prefix + '.pkl')
        with open(file_path, "wb") as f:
            pickle.dump(data, f)
        rich.print(f'\t Saved file: [italic yellow]{file_path}[/]')


if __name__ == '__main__':
    API_URL = 'https://api.dandiarchive.org/api/dandisets'
    DATA_DIR = 'data/dandiarchive/raw'
    
    # DEFINE CONFIG
    cfg = dict(
        to_json=True,
        retries=5,
        max_keepalive_connections=50,
        max_connections=80,
        keepalive_expiry=10,
        timeout=20,
        follow_redirects=True
    )
    
    # DEFINE POTENTIAL RANGE & BATCH SCRAPE
    max_dandiset_ID = 1140 # at the time of collection: 2024-08-23, the max is around 891
    batch_size = 100 # break into chunks to avoid potential limits
    
    # Construct list of batch
    all_IDs = range(max_dandiset_ID)
    batch_IDs = [
        all_IDs[i:i+batch_size]
        for i in range(0, len(all_IDs), batch_size)
    ]
    
    for IDs in tqdm(batch_IDs, desc='Batch scrape'):
        id_range = '%06d-%06d' %(min(IDs), max(IDs))
        rich.print('>>> [bold green]START:[/][italic] Dandiset[/] [bold green]%s[/]' %(id_range))
        file_suff = 'dandiset-range_' + id_range
        
        # 1. GET VERSION: Go through all possible IDs and find all versions
        # 1.1. Construct URLs
        version_urls = [f'{API_URL}/{ID:06d}/versions' for ID in IDs]

        # 1.2. Scrape
        version_data = asyncio.run(scrape(
            version_urls,
            desc='VERSIONS',
            **cfg
        ))

        # 1.3. Save
        save_data(
            data=version_data, 
            file_prefix='versions_' + file_suff,
            data_directory=DATA_DIR
        )

        # 2. GET METADATA: Obtain metadata from available dandisets
        # 2.1. Get only the available dandiset and construct URLs
        # e.g. URLs that need auth would return status != 200
        dandiset_urls = pd.DataFrame(version_data)\
            .query('status == 200')['response']\
            .apply(lambda x: x['results']).explode()\
            .apply(lambda x: '%s/%s/versions/%s' %(
                API_URL,
                x['dandiset']['identifier'],
                x['version']
            ))\
            .to_list()
        
        # 2.2. Scrape
        dandiset_metadata = asyncio.run(scrape(
            dandiset_urls,
            desc='METADATA',
            **cfg
        ))

        # 2.3. Save
        save_data(
            data=dandiset_metadata, 
            file_prefix='metadata_' + file_suff,
            data_directory=DATA_DIR
        )
        
        # 3. GET ASSET LIST: Obtain list of assets for each URL
        # 3.1. Construct URLs
        assetls_urls = [f'{x}/assets' for x in dandiset_urls]

        # 3.2. Scrape
        asset_lists = asyncio.run(scrape(
            assetls_urls,
            desc='ASSET LIST',
            **cfg
        ))

        # 2.3. Save
        save_data(
            data=asset_lists, 
            file_prefix='assetlist_' + file_suff,
            data_directory=DATA_DIR
        )
        
        rich.print('<<< [bold green]DONE:[/][italic] Dandiset[/] [bold green]%s[/]' %(id_range))
    
    rich.print('[bold green] ALL DONE[/]')
