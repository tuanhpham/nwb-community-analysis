import asyncio
import httpx

import rich
import rich.progress

async def get_response(
    client, 
    url,
    progress,
    task, 
    to_json=True
):
    response = await client.get(url)
    data = dict(
        url = url,
        status = response.status_code, 
    )
    if to_json:
        data['response'] = response.json()
    else:
        data['response'] = response.content
        
    if progress is not None:
        assert task is not None
        progress.update(task, advance=1)

    return data

async def scrape(
    urls, 
    desc=None,
    to_json=True,
    retries=5,
    max_keepalive_connections=30,
    max_connections=50,
    keepalive_expiry=5,
    timeout=20,
    follow_redirects=True
):
    pbar_args = [
        rich.progress.TextColumn(
            "[green]{task.description}[/green]", table_column=rich.table.Column(ratio=1)
        ),
        "[white][progress.percentage]{task.percentage:>3.0f}%[/white]",
        rich.progress.BarColumn(
            bar_width=None, table_column=rich.table.Column(ratio=2)
        ),
        rich.progress.TextColumn(
            "[green][{task.completed}/{task.total}][/green]",
            table_column=rich.table.Column(ratio=1),
        ),
        rich.progress.SpinnerColumn(table_column=rich.table.Column(ratio=1))
    ]
    
    client_kargs = dict(    
        transport=httpx.AsyncHTTPTransport(retries=retries),
        limits=httpx.Limits(
            max_keepalive_connections=max_keepalive_connections,
            max_connections=max_connections,
            keepalive_expiry=keepalive_expiry,
        ),
        timeout=httpx.Timeout(timeout),
        follow_redirects=follow_redirects
    )
    
    if desc is None:
        desc = ''
    else:
        desc = desc + ' '
    
    with rich.progress.Progress(*pbar_args) as progress:
        task = progress.add_task(f"{desc}Requesting ...", total=len(urls))

        async with httpx.AsyncClient(**client_kargs) as client:
            data = await asyncio.gather(*[
                asyncio.ensure_future(
                    get_response(
                        client, url, progress, task, 
                        to_json=to_json
                    ))
                for url in urls
            ])
            
    rich.print(f"{desc}Finished.")

    return data
