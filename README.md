# Analysis of related community and data around NWB standard

This contains some random analysis of the community, issues around the NWB (Neurodata Without Borders) standard, including DANDIArchive as a potential data repository.

## Requirements

- `python3`
- `make`
- `curl`

To install Python requirements do:

```shell
pip install -r requirements.txt
```

## Staged NDX PRs

This part collects the PRs of [`nwb-extensions/staged-extensions`](https://github.com/nwb-extensions/staged-extensions) and visualizes the timeline of merged / closed / opened PRs. Merged PRs are usually accepted extensions.

### 1. Collect data

First, collect the data:

```shell
make get-staged-ndx-GH-PR
make get-staged-ndx-PR-files
```

This uses Github API to collect PR statuses and files to be reviewed from these PRs (to better obtain extension names).

The output data are saved in `data/github`.

Note: this does not account for Github API limit, and at the moment there are not many to reach limits yet.

### 2. Visualization

Then, head over `notebooks/A-visualize-staged-ndx-GH-PR.ipynb` notebook for data processing and timeline visualization.

The figure files will be in `figures/staged-ndx`.


## DANDI Archive contributor affiliations

This part collects metadata from [DANDI Archive](https://dandiarchive.org) and analyzes the affiliations of contributors of dandisets.

### 1. Collect (meta)data

First, collect the metadata of dandisets:

```shell
make scrape-dandi-meta
```

This will collect the metadata of dandisets and save to `data/dandiarchive/raw`.

Note: The Python API package `dandi` actually is a much much better way of doing this.

### 2. Preprocess metadata and filter S3 URLs

Run the notebook `notebooks/B1-filter-dandi-s3-urls.ipynb` to pre-process the collected raw metadata in `data/dandiarchive/raw` and outputs data into `data/dandiarchive/proc/filtered-dandiset-metadata-assets.pkl`.

The other output `data/dandiarchive/proc/filtered-S3-URLs.txt` is a list of S3 URLs, basicially the S3 URLs to the first NWB asset for each dandiset that has NWB data. The purpose of this is because some dandisets may lack information about contributors, hence reading into NWB files, at least one per dandiset, helps.

### 3. Read S3 data

The two following notebooks read the S3 URL links on DandiHub (aka the Jupyter Hub offered by DANDI Archive) to extract lab / people / instiution information.

- `notebooks/B2-dandihub-read-S3-use-pynwb.ipynb`
- `notebooks/B2-dandihub-read-S3-use-h5.ipynb`

These can actually run anywhere, instead of DandiHub. The reason for DandiHub is because this is hosted on AWS, and the NWB files of dandisets are hosted on AWS (hence S3 URLs). This means that reading on DandiHub would generally be faster.

Only one of these notebooks is actually needed for the data about affiliations / institutes. It's more recommended to do the `h5` one instead of `pynwb` one in this case because the former is much faster and the types / locations of data are defined here. The latter is only relevant to for speed comparison if need be.

These two notebooks requires the file `data/dandiarchive/proc/filtered-S3-URLs.txt` as input, then output the following:

- `filter-h5-read.{json,pkl}` (from `h5` nb)
- `filter-nwb-affiliation.{json,pkl}` (from `pynwb` nb)

### 4. Analyze

The notebook `notebooks/B3-visualize-affiliation-metadata.ipynb` can then be run to analyze and visualize the affiliations / contribitions of the NWB datasets deposited on DANDI Archive.

The output figures are saved at `figures/dandi-contrib`.
